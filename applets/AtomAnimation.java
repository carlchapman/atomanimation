package applets;

import java.lang.reflect.InvocationTargetException;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class AtomAnimation implements Runnable {
	final public AtomPanel panel;
	final private JFrame frame;
	public AtomAnimation(AtomPanel p,JFrame f) {
		panel=p;
		frame=f;
	}

	@Override
	public void run() {
		while (true) {
	        try {
	        	Thread.sleep(60L);
				SwingUtilities.invokeAndWait(new Runnable() {
				    public void run() {
					 	panel.advanceTime(40L); 
						frame.repaint();
				    }
				});
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
