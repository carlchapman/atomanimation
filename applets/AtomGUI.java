package applets;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AtomGUI {	
	
	final static ExecutorService exService = Executors.newSingleThreadExecutor();
	
	public static void main(String[] args){
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                beginAnimation(); 
            }
        });
	}
	
	public static void beginAnimation(){
		final JFrame frame = new JFrame("Atom Animation");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		AtomPanel panel = new AtomPanel(frame);
		panel.setBackground(Color.WHITE);
		frame.getContentPane().add(BorderLayout.CENTER,panel);
		frame.setSize(new Dimension(400,400));
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
		exService.execute(new AtomAnimation(panel,frame));
	}
}
