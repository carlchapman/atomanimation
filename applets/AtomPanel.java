package applets;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class AtomPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private static final double COLLISION_THRESHOLD = 10;
	private Atom[] atoms;
	private Electron[][][] electrons;
	private int numberOfAtoms = 25;
	private int numberOfElectrons = 32;
	private Random gen = new Random(System.currentTimeMillis());
	private JFrame parent;
	private long orbitTime = 0L;

	public AtomPanel(JFrame frame) {
		parent = frame;
		setBorder(BorderFactory.createLineBorder(Color.black));
		setupAtoms();
	}
	
	public void changeBackground(){
		this.setBackground(new Color(gen.nextFloat(),gen.nextFloat(),gen.nextFloat()));
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		orbitTime+=13;
		long t= orbitTime;

		// orbit the electrons
		for (int i = 0; i < numberOfAtoms; i++) {
			for (int j = 0; j < 4; j++) {
				for (int k = 0; k < electrons[i][j].length; k++) {
					
					electrons[i][j][k]
							.setCenter(
									polarX(t / 5 + 0.25 * Math.PI * k,
											Electron.RADIUS - 5, 3 + 1.5 * j,
											atoms[i]),
									polarY(t / 5 + 0.25 * Math.PI * k,
											Electron.RADIUS - 5, 3 + 1.5 * j,
											atoms[i]));
					g.setColor(electrons[i][j][k].getColor());
					g.fillOval((int) electrons[i][j][k].X,
							(int) electrons[i][j][k].Y,
							(int) electrons[i][j][k].getRadius(),
							(int) electrons[i][j][k].getRadius());
					g.setColor(new Color(0, 0, 0));
					g.drawOval((int) electrons[i][j][k].X,
							(int) electrons[i][j][k].Y,
							(int) electrons[i][j][k].getRadius(),
							(int) electrons[i][j][k].getRadius());
				}
			}
		}
	}

	private static double polarX(double th, double a, double n, Atom B) {
		double r = a * Math.sin(n * th);
		double x = r * Math.cos(th);
		double T = x + B.getXPosition();
		return T;
	}

	private static double polarY(double th, double a, double n, Atom B) {
		double r = a * Math.sin(n * th);
		double y = r * Math.sin(th);
		double T = y + B.getYPosition();
		return T;
	}

	private void setupAtoms() {
		int aWidth = 2*Electron.RADIUS;
		int width = parent.getSize().width-2*aWidth;
		int height = parent.getSize().height-2*aWidth;
		electrons = new Electron[numberOfAtoms][4][];
		atoms = new Atom[numberOfAtoms];
		for (int z = 0; z < numberOfAtoms; z++) {
			double q = gen.nextDouble() / 5;
			atoms[z] = new Atom(width / numberOfAtoms * z, height
					/ numberOfAtoms * z, q, q);
			int a = 0, b = 0, c = 0, d = 0;
			for (int i = 0; i < numberOfElectrons; i++) {
				if (i % 4 == 0) {
					a++;
				} else if (i % 4 == 1) {
					b++;
				} else if (i % 4 == 2) {
					c++;
				} else if (i % 4 == 3) {
					d++;
				}
			}

			electrons[z][0] = new Electron[a];
			for (int j = 0; j < a; j++) {
				electrons[z][0][j] = new Electron(createElectronColor(z, 0, a),
						width / numberOfAtoms * z, height / numberOfAtoms * z,
						Electron.RADIUS);

			}
			electrons[z][1] = new Electron[b];
			for (int j = 0; j < b; j++) {
				electrons[z][1][j] = new Electron(createElectronColor(z, 1, b),
						width / numberOfAtoms * z, height / numberOfAtoms * z,
						Electron.RADIUS);

			}
			electrons[z][2] = new Electron[c];
			for (int j = 0; j < c; j++) {
				electrons[z][2][j] = new Electron(createElectronColor(z, 2, c),
						width / numberOfAtoms * z, height / numberOfAtoms * z,
						Electron.RADIUS);

			}
			electrons[z][3] = new Electron[d];
			for (int j = 0; j < d; j++) {
				electrons[z][3][j] = new Electron(createElectronColor(z, 3, d),
						width / numberOfAtoms * z, height / numberOfAtoms * z,
						Electron.RADIUS);

			}
		}

	}

	private Color createElectronColor(int i, int j, int k) {
		int n = 4, l = 8;
		int o = numberOfElectrons;
		if (i % 8 == 0) {
			return new Color(105 / o * i, 255 / n * j, 105 / o * i); // green
																		// gradients
		}
		if (i % 8 == 1) {
			return new Color(20 / o * i, 0, 255 - 255 / l * k); // blue to black
		}
		if (i % 8 == 2) {
			return new Color(120 / o * i + 45 / n * j + 55 / l * k, 30, 120 / o
					* i + 45 / n * j + 55 / l * k); // purples //green to purple
		}
		if (i % 8 == 3) {
			return new Color(200 - 70 / n * j, 160 - 105 / l * k, 80); // browns
		}
		if (i % 8 == 4) {
			return new Color(255, 255 - 75 / l * k, 0); // yellow and red with
														// more yellow
		}
		if (i % 8 == 5) {
			return new Color(155 / l * k, 155 / n * j, 255); // light blues
		}
		if (i % 8 == 6) {
			return new Color(230 - 60 / l * k, 140 / o * i, 30 / o * i); // reds
		}
		if (i % 8 == 7) {
			return new Color(255, 255 - 225 / l * k, 0); // yellow and red with
															// more red
		}
		return Color.BLUE;
	}

	public void advanceTime(long t) {
		int width = parent.getSize().width-Electron.RADIUS;
		int height = parent.getSize().height-(int)(2.5*Electron.RADIUS);
		// /move the atoms
		// move each particle, then bounce it off the walls
		for (int i = 0; i < numberOfAtoms; i++) {

			atoms[i].run(t);
			// bounce the particle off the walls
			atoms[i].bounceOffWall(0, 0, width, height);
			//System.out.println("atom position: "+atoms[i].getXPosition()+","+atoms[i].getYPosition());
		}
		// initialize variables used to determine which particle to collide
		// during this time step
		double minimumDistance = 10.1;
		int closestIndex1 = 0;
		int closestIndex2 = 0;

		// accept only potential collisions who are within the collision
		// threshold, are moving towards each other, and if there is a barrier,
		// only accept partners that are on the same side of the barrier. If a
		// pair of particles passes those tests, test it to find the indices of
		// the closest two particles.
		for (int i = 0; i < numberOfAtoms; i++) {
			for (int j = 0; j < numberOfAtoms; j++) {
				if (i != j) {
					if (atoms[i].getDistance(atoms[j]) < COLLISION_THRESHOLD) {
						if (atoms[i].isRunningTowards(atoms[j])) {
							if (atoms[i].getDistance(atoms[j]) < minimumDistance) {
								minimumDistance = atoms[i]
										.getDistance(atoms[j]);
								closestIndex1 = i;
								closestIndex2 = j;
							}
						}
					}
				}
			}
		}
		// collide the closest two particles
		atoms[closestIndex1].collide(atoms[closestIndex2]);
		
	}

}
