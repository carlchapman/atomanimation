package applets;

public class Atom {
	/**
	 * The position vector.
	 */
	private double X;
	private double Y;
	/**
	 * The velocity vector.
	 */
	private double xV;
	private double yV;

	/**
	 * Constructs an Atom with a given position and velocity
	 */
	public Atom(double initialXPosition, double initialYPosition,
			double initialXVelocity, double initialYVelocity) {
		// initialize the particle's position and velocity vectors.
		this.X = initialXPosition;
		this.Y = initialYPosition;
		this.xV = initialXVelocity;
		this.yV = initialYVelocity;
		// this.electrons =electrons;

		// this.numberOfElectrons = numberOfElectrons;
		// setupAtoms();
	}

	/**
	 * gets the position vector of the particle.
	 * 
	 * @return the position vector of the particle.
	 */
	public double getXPosition() {
		return X;
	}

	/**
	 * gets the position vector of the particle.
	 * 
	 * @return the position vector of the particle.
	 */
	public double getYPosition() {
		return Y;
	}

	/**
	 * gets the velocity vector of the particle.
	 * 
	 * @return the velocity vector of the particle.
	 */
	public double getXVelocity() {
		return xV;
	}

	/**
	 * gets the velocity vector of the particle.
	 * 
	 * @return the velocity vector of the particle.
	 */
	public double getYVelocity() {
		return yV;
	}

	/**
	 * gets the distance between two particles.
	 * 
	 * @param other
	 *            the other particle from which the distance is calculated.
	 * @return the distance between this particle and the other particle.
	 */

	public double getDistance(Atom other) {

		// find the distance using the distance formula
		return Math.sqrt((other.X - this.X) * (other.X - this.X)
				+ (other.Y - this.Y) * (other.Y - this.Y));
	}

	/**
	 * determines if the distance between two particles is getting smaller.
	 * 
	 * @param other
	 *            the other particle that might or might not be getting closer
	 *            to this particle.
	 * @return true if the particles are getting closer.
	 */
	public boolean isRunningTowards(Atom other) {
		double distanceVectorX = this.X - other.X;
		double distanceVectorY = this.Y - other.Y;
		double velocityVectorX = this.xV - other.xV;
		double velocityVectorY = this.yV - other.yV;

		double dotProduct = velocityVectorX * distanceVectorX + velocityVectorY
				* distanceVectorY;
		return dotProduct < 0;
	}

	/**
	 * collide the two particles, exchanging their velocity vectors
	 * 
	 * @param other
	 *            the other particle with which this particle will collide
	 */
	public void collide(Atom other) {
		double PI = Math.PI;
		double A, D1, D2, v1x, v1y, v2x, v2y, v1, v2, f1x, f2x;
		A = Math.atan2(this.Y - other.Y, this.X - other.X);
		v1 = Math.sqrt(this.xV * this.xV + this.yV * this.yV);
		v2 = Math.sqrt(other.xV * other.xV + other.yV * other.yV);
		D1 = Math.atan2(this.yV, this.xV);
		D2 = Math.atan2(other.yV, other.xV);
		v1x = v1 * Math.cos(D1 - A);
		v1y = v1 * Math.sin(D1 - A);
		v2x = v2 * Math.cos(D2 - A);
		v2y = v2 * Math.sin(D2 - A);
		f1x = v2x;
		f2x = v1x;
		this.xV = Math.cos(A) * f1x + Math.cos(A + PI / 2) * v1y;
		this.yV = Math.sin(A) * f1x + Math.sin(A + PI / 2) * v1y;
		other.xV = Math.cos(A) * f2x + Math.cos(A + PI / 2) * v2y;
		other.yV = Math.sin(A) * f2x + Math.sin(A + PI / 2) * v2y;
	}

	public void run(long timeStep) {
		// set the position x and y to a new position adding the new distance
		this.X = X + xV * timeStep;
		this.Y = Y + yV * timeStep;
	}

	/**
	 * bounces a particle off of a container wall
	 * 
	 * @param x
	 *            the position of the x origin point
	 * @param y
	 *            the position of the y origin point
	 * @param width
	 *            the width of the box
	 * @param height
	 *            the height of the box
	 */
	public void bounceOffWall(double x, double y, double width, double height) {
		// bounce off the wall formed by the line x=0
		if (this.X < 0) {
			this.xV = -this.xV;
			this.X = -this.X;
		}
		// bounce off the wall formed by the line x=width
		if (this.X > width) {
			this.xV = -this.xV;
			this.X = (width - (this.X - width));
		}
		// bounce off the wall formed by the line y=0
		if (this.Y < 0) {
			this.yV = -this.yV;
			this.Y = -this.Y;
		}
		// bounce off the wall formed by the line y=width
		if (this.Y > height) {
			this.yV = -this.yV;
			this.Y = height - (this.Y - height);
		}
	}

}
