package applets;

import java.awt.Color;

public class Electron {
	public static final int RADIUS = 15;
	public double X;
	public double Y;
	
	/**
	 * The color of the electron.
	 */
	private Color color;

	/**
	 * The radius of the circle.
	 */
	private double radius;
/**
 * Constructs a electron with a given initial color, position and radius.
 * @param initialColor
 * Sets the initial color.
 * @param initalPosition
 * Sets the initial Position of the electron.
 * @param initialRadius
 * Sets the initial radius of the electron.
 */
	public Electron(Color initialColor, double initialX,double initialY, double initialRadius){
		this.X=initialX;
		this.Y=initialY;
		this.color=initialColor;
		this.radius=initialRadius;	
	}
	/**
	 * Gets the Color of the electron.
	 * @return
	 * Electron color.
	 */
	public Color getColor(){
		return color;
	}
	/**
	 * Gets the radius of the electron.
	 * @return
	 * Electron radius.
	 */
	public double getRadius(){
		return radius;
	}
	/**
	 * Sets a new center position for the electron.
	 */
	public void setCenter(double newX,double newY){
		X=newX;
		Y=newY;
	}
}
